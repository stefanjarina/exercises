﻿module Bob
open System

let response (input: string): string = 
  let silent: bool = String.IsNullOrWhiteSpace(input)
  let letters: bool = Seq.exists Char.IsLetter input
  let question: bool = input.Trim().EndsWith "?"
  let shouting: bool = letters && input = input.ToUpperInvariant()

  match input with
    | _ when silent -> "Fine. Be that way!"
    | _ when shouting && question -> "Calm down, I know what I'm doing!"
    | _ when shouting -> "Whoa, chill out!"
    | _ when question -> "Sure."
    | _ -> "Whatever."
