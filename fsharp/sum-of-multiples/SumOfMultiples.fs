﻿module SumOfMultiples

let sum (numbers: int list) (upperBound: int): int =
    let isMultiple (n : int): bool = numbers |> List.exists (fun l -> l <> 0 && n % l = 0)

    [1 .. upperBound - 1]
    |> List.filter isMultiple
    |> List.sum
