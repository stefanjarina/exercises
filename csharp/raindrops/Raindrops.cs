using System;

public static class Raindrops
{
    public static string Convert(int number)
    {
        string s = "";
        s += number%3 == 0 ? "Pling" : "";
        s += number%5 == 0 ? "Plang" : "";
        s += number%7 == 0 ? "Plong" : "";
        return s == "" ? number.ToString() : s;
    }
}
