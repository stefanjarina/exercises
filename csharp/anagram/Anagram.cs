using System;
using System.Linq;

public class Anagram
{
    private string _baseWord;
    private char[] _standardizedBaseWord;

    public Anagram(string baseWord)
    {
        _baseWord = baseWord;
        _standardizedBaseWord = this.standardizeWord(_baseWord);
    }

    public string[] FindAnagrams(string[] potentialMatches)
    {
        return potentialMatches.Select(m => m).Where(n => !this.isSame(n) && this.compareWords(n)).ToArray();
    }

    private bool isSame(string candidate)
    {
        return _baseWord.ToLower() == candidate.ToLower();
    }

    private char[] standardizeWord(string word)
    {
        return word.ToLower().OrderBy(c => c).ToArray();
    }

    private bool compareWords(string candidate)
    {
        return _standardizedBaseWord.SequenceEqual(this.standardizeWord(candidate));
    }
}
