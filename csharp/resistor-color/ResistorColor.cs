﻿using System;

public static class ResistorColor
{
    enum ResistorColors { black, brown, red, orange, yellow, green, blue, violet, grey, white };
    public static int ColorCode(string color)
    {
        ResistorColors yourEnum;
        var result = Enum.TryParse(color, out yourEnum);
        if (result)
            return (int)yourEnum;
        throw new ArgumentOutOfRangeException("This color is not available");
    }

    public static string[] Colors()
    {
        throw new NotImplementedException("You need to implement this function.");
    }
}
