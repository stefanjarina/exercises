using System;
using System.Collections.Generic;

public class Robot
{
    private static List<string> usedNames = new List<string>();
    private string _name;

    public Robot()
    {
        assignName();
    }

    public string Name
    {
        get
        {
            return _name;
        }
    }

    public void Reset()
    {
        assignName();
    }

    private void assignName()
    {
        string newName = generateName(2, 3);
        if (usedNames.Contains(newName))
            assignName();
        usedNames.Add(newName);
        _name = newName;
    }

    private string generateName(int letterslength, int numbersLength)
    {
        var letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var numbers = "0123456789";
        var result = "";
        var random = new Random();

        for (int i = 0; i < letterslength + numbersLength; i++)
        {
            if (i < letterslength) {
                result += letters[random.Next(letters.Length)];
            }
            else
            {
                result += numbers[random.Next(numbers.Length)];
            }
        }
        return result;
    }
}
