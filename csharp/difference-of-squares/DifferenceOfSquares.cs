﻿using System;

public static class DifferenceOfSquares
{
    public static int CalculateSquareOfSum(int n)
    {
        var result = (n * (n + 1)) / 2;
        return result * result;
    }

    public static int CalculateSumOfSquares(int n)
    {
        return (n * (n + 1) * (2*n + 1)) / 6;
    }

    public static int CalculateDifferenceOfSquares(int n)
    {
        return CalculateSquareOfSum(n) - CalculateSumOfSquares(n);
    }
}
