﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class SumOfMultiples
{
    public static int Sum(IEnumerable<int> multiples, int max)
    {
        return Enumerable.Range(1, max-1).Aggregate(0, (acc, x) => isMultiple(multiples, x) ? acc + x : acc );
    }

    private static bool isMultiple(IEnumerable<int> list, int n) {
        return list.Where(i => i > 0).Any(item => n % item == 0 );
    }
}
