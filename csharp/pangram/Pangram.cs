﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class Pangram
{
    public static bool IsPangram(string input)
    {
        // The aToZ can be probably hardcoded as it is known to use ASCII letters a-z
        List<string> aToZ = Enumerable.Range('a', 26).Select(x => ((char)x).ToString()).ToList();
        
        string normalizedInput = input.ToLower();
        return aToZ.All(itm1 => normalizedInput.Contains(itm1));
    }
}
