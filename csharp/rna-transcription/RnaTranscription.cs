using System;
using System.Collections.Generic;
using System.Linq;

public static class RnaTranscription
{

    public static string ToRna(string nucleotide)
    {
        IDictionary<char, char> dnaToRna = new Dictionary<char, char>();
        dnaToRna.Add('G', 'C');
        dnaToRna.Add('C', 'G');
        dnaToRna.Add('T', 'A');
        dnaToRna.Add('A', 'U');

        return String.Join("", nucleotide.Select(i => dnaToRna.ContainsKey(i) ? dnaToRna[i] : i));
    }
}
