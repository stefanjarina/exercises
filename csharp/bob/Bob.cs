using System;
using System.Text.RegularExpressions;

public static class Bob
{
    public static string Response(string statement)
    {
        statement = statement.Trim();
        if (String.IsNullOrEmpty(statement))
        {
            return "Fine. Be that way!";
        }
        else if (statement == statement.ToUpper() && isLetters(statement) && statement.EndsWith('?'))
        {
            return "Calm down, I know what I'm doing!";
        }
        else if (statement == statement.ToUpper() && isLetters(statement))
        {
            return "Whoa, chill out!";
        }
        else if (statement.EndsWith('?'))
        {
            return "Sure.";
        }
        else
        {
            return "Whatever.";
        }
    }

    private static bool isLetters(string statement)
    {
        Regex regex = new Regex(@"[A-Z]");
        return regex.IsMatch(statement);
    }
}
