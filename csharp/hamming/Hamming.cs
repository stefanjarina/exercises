using System;
using System.Linq;

public static class Hamming
{
    public static int Distance(string firstStrand, string secondStrand)
    {
        if (firstStrand.Length != secondStrand.Length) {
            throw new ArgumentException("DNA strands must be of equal length.");
        }
        return Enumerable.Range(0, firstStrand.Length).Aggregate(0, (acc, x) =>
                        firstStrand[x] != secondStrand[x] ? acc += 1 : acc);
    }
}
