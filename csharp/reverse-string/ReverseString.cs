﻿using System;

public static class ReverseString
{
    public static string Reverse(string input)
    {
        if (String.IsNullOrEmpty(input)) return "";
        char[] array = input.ToCharArray();
        Array.Reverse(array);
        return new String(array);
    }
}
