using System;

public static class Gigasecond
{
    const double GigasecondInSeconds = 1e9;
    public static DateTime Add(DateTime birthDate)
    {
        return birthDate.AddSeconds(GigasecondInSeconds);
    }
}
