using System;

public class SpaceAge
{
    private const double EARTH_SECONDS = 31557600;
    private long _age;

    public SpaceAge(long seconds)
    {
        _age = seconds;
    }

    public double OnEarth()
    {
        return Math.Round(_age / EARTH_SECONDS, 2);
    }

    public double OnMercury()
    {
        return Math.Round(_age / (EARTH_SECONDS * 0.2408467), 2);
    }

    public double OnVenus()
    {
        return Math.Round(_age / (EARTH_SECONDS * 0.61519726), 2);
    }

    public double OnMars()
    {
        return Math.Round(_age / (EARTH_SECONDS * 1.8808158), 2);
    }

    public double OnJupiter()
    {
        return Math.Round(_age / (EARTH_SECONDS * 11.862615), 2);
    }

    public double OnSaturn()
    {
        return Math.Round(_age / (EARTH_SECONDS * 29.447498), 2);
    }

    public double OnUranus()
    {
        return Math.Round(_age / (EARTH_SECONDS * 84.016846), 2);
    }

    public double OnNeptune()
    {
        return Math.Round(_age / (EARTH_SECONDS * 164.79132), 2);
    }
}
