#!/usr/bin/env bash

IN=$1

if [[ "$IN" -eq "" ]]; then
    echo "Usage: $(basename "$0") <number>"
    exit -1
fi

numbers=$(echo "$IN" | awk NF=NF FS=)
length=${#IN}

for i in $numbers; do
    (( result += $((i ** length)) ))
done

if [[ "$IN" -eq "$result" ]]; then
    echo "true"
else
    echo "false"
fi
