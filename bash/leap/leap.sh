#!/usr/bin/env bash

if [[ ! "$#" -eq 1 ]] || [[ ! "$1" =~ ^[0-9]+$ ]]; then
    echo "Usage: $(basename "$0") <year>"
    exit 1
fi

year=$1

if [[ $(( year % 4)) -eq 0 ]] && [[ ! $(( year % 100)) -eq 0 ]] || [[ $(( year % 400)) -eq 0 ]]; then
    echo "true"
else
    echo "false"
fi