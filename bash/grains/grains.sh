#!/usr/bin/env bash

input="$1"

get_grains () {
  local index=$(($1-1))
  printf "%u\n" "$(( 2 ** index ))"
}

get_total_grains() {
  printf "%u\n" "$(((2**64)-1))"
}

if [[ $input == "total" ]]; then
  get_total_grains
elif [[ $input -le 0 || $input -gt 64 ]]; then
  echo "Error: invalid input"
  exit 1
else
  get_grains "$input"
fi
