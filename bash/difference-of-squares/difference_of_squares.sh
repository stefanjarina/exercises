#!/usr/bin/env bash

n="$2"

square_of_sum () {
  sum=$(( (n * (n+1)) / 2 ))
  echo $(( sum * sum ))
}

sum_of_squares () {
  echo $(( (n * (n + 1) * ((2*n) + 1)) / 6))
}

difference () {
  echo $(($(square_of_sum) - $(sum_of_squares)))
}

"$@"