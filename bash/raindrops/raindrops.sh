#!/usr/bin/env bash

if [[ ! "$#" -eq 1 ]] || [[ ! "$1" =~ ^[0-9]+$ ]]; then
    echo "Usage: $(basename "$0") <number>"
    exit 1
fi

(( $1%3 == 0)) && output+=Pling
(( $1%5 == 0)) && output+=Plang
(( $1%7 == 0)) && output+=Plong
echo ${output:-$1}