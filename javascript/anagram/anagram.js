"use strict";

function anagram(word) {
	return {
		matches: matches.bind(this, word)
	};
}

function standardize(word){
	return word.toLowerCase().split("").sort().toString();
}

function same(candidate, word){
	return candidate.toLowerCase() === word.toLowerCase();
}

function compare(candidate, word){
	return standardize(candidate) === standardize(word);
}

function matches(word, candidates){
	candidates = Array.isArray(candidates) ? candidates : [].slice.call(arguments, 1);

	return candidates.filter(function(candidate){
		return !same(candidate, word) && compare(candidate, word);
	});
}

module.exports = anagram;