function Bob(){

  this.hey = function(input) {
    if(input.trim() === ''){
      return 'Fine. Be that way!';
    }else if(input === input.toUpperCase() && /[A-Z]/.test(input)){
      return 'Whoa, chill out!';
    }else if(input[input.length-1] === '?' ){
      return 'Sure.';
    }else{
      return 'Whatever.';
    }
  };
}

module.exports = Bob;