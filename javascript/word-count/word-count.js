function WordCounter(text){
    var output = {};
    var splitedText = text.split(/\s+/);
    splitedText.forEach(function(word){
        output[word] = output.hasOwnProperty(word) ? output[word] + 1 : 1;
    });
    return output;
}

module.exports = WordCounter;