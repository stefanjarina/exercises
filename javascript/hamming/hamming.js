exports.compute = function (stringA, stringB) {
	var lenA = stringA.length;
	var lenB = stringB.length;
	if (lenA !== lenB) {
		throw 'DNA strands must be of equal length.';
	}
	var counter = 0;
	for(var i = 0; i < lenA; i++) {
		if(stringA[i] !== stringB[i]) counter++;
	}
	return counter;
};