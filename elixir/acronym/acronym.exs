defmodule Acronym do
  @doc """
  Generate an acronym from a string.
  "This is a string" => "TIAS"
  """

  @spec abbreviate(string) :: String.t()
  def abbreviate(string) do
    string
      |> String.split
      |> Enum.map(&remove_punctuation(&1))
      |> Enum.map(&split_inconsistent_case(&1))
      |> List.flatten
      |> Enum.map_join(&String.first(&1))
      |> String.upcase
  end

  defp remove_punctuation(string) do
    Regex.replace(~r/[.,\/#!$%\^&\*;:{}=\-_`~()]/, string, "")
  end

  defp split_inconsistent_case(string) do
    # TO-DO: Fix regex to not produce [""] - not sure why it does though
    Regex.scan(~r/[A-Z]?[a-z]*/, string) |> List.delete([""])
  end
end
