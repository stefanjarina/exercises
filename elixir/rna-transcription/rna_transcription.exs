defmodule RNATranscription do
  @doc """
  Transcribes a character list representing DNA nucleotides to RNA

  ## Examples

  iex> RNATranscription.to_rna('ACTG')
  'UGAC'
  """

  @dna_to_rna_map %{
    'G' => 'C',
    'C' => 'G',
    'T' => 'A',
    'A' => 'U'
  }

  @spec to_rna([char]) :: [char]
  def to_rna(dna) do
    dna
    |> Char.split('', trim: true)
    |> Enum.map(fn x -> @dna_to_rna_map[x] end)
    |> Enum.join
  end
end
