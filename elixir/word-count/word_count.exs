defmodule Words do
  @ascii_punctuation ~r/!|"|\#|\$|%|&|'|\(|\)|\*|\+|,|\.|\/|:|;|<|=|>|\?|@|\[|\\|]|\^|_|`|\{|\||}|~/

  @doc """
  Count the number of words in the sentence.

  Words are compared case-insensitively.
  """
  @spec count(String.t) :: map()
  def count(sentence) do
    sentence
    |> String.downcase
    |> to_words
    |> Enum.reduce(%{}, &count/2)
  end

  defp to_words(sentence) do
    Regex.replace(@ascii_punctuation, sentence, " ")
    |> String.split
    |> List.flatten
  end

  defp count(word, counts) do
    Dict.update(counts, word, 1, &(&1 + 1))
  end
end
