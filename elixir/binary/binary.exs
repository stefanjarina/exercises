defmodule Binary do
  @doc """
  Convert a string containing a binary number to an integer.

  On errors returns 0.
  """
  @spec to_decimal(String.t) :: non_neg_integer
  def to_decimal(string) do

    if !is_valid?(string) do
      0
    else
      string
      |> String.split("", trim: true)
      |> Enum.reverse
      |> Enum.with_index()
      |> Enum.reduce(0, fn({char, index}, acc) ->
          case char do
            "1" -> acc + :math.pow(2, index) |> round
            "0" -> acc
          end
      end)
    end
  end

  defp is_valid?(string) do
    Regex.match?(~r/^[01]+$/, string)
  end
end
