class Prime

  def self.nth(n)
    raise ArgumentError if n < 1

    i = 1
    prime = 0
    while prime < n do
      i += 1
      prime += 1 if is_prime? i
    end
    i
  end

  private
  def is_prime?(n)
    return false if n == 1
    return true if n == 2
    (2..Math.sqrt(n).ceil).each do |i|
      return false if n % i == 0
    end
    return true
  end
end