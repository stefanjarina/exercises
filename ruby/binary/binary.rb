class Binary
  VERSION = 1
  def initialize(binary)
    raise ArgumentError unless binary.match(/^[01]+$/)
    @binary = binary.chars.reverse
  end

  def to_decimal
    result = 0
    @binary.each_with_index {|char, index| result += 2**index if char == "1" }
    result
  end
end