class Complement
  VERSION = 3
  @dna_to_rna = {
    "G" => "C",
    "C" => "G",
    "T" => "A",
    "A" => "U",
  }
  def Complement.of_dna(dna)
    raise ArgumentError unless dna.match(/^[GCTA]+$/)
    dna.chars.map{|d| @dna_to_rna[d]}.join
  end
end