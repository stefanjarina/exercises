class ETL
  def self.transform(old_data)
  new_data = {}
  old_data.each{|score, letters| letters.each{|letter| new_data[letter.downcase] = score} }
  new_data
  end
end