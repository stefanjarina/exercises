class SumOfMultiples
  def initialize(*multiples)
    @multiples = multiples
  end

  def to(number)
    result = 0
    (1...number).each{|n| result += n if multiple? n}
    result
  end

  def self.to(n)
    new(3, 5).to(n)
  end

  private
  def multiple?(n)
    @multiples.any? {|m| n % m == 0}
  end
end