class Robot
  attr_reader :name

  def initialize
    @name = generate_name
  end

  def reset
    @name = generate_name
  end

  private
  def generate_name
    name = ""
    name += ('A'..'Z').to_a.shuffle[0,2].join
    name += ('0'..'9').to_a.shuffle[0,3].join
    name
  end
end