class Series
  def initialize(string)
    @string = string.chars.to_a.map(&:to_i)
  end

  def slices(n)
    raise ArgumentError if @string.length < n
    list = []
    i = 0
    while i < @string.length - (n-1) do
      list << @string[i..i+(n-1)]
      i += 1
    end
    list
  end
end