class Trinary
  def initialize(trinary)
    @trinary = trinary.match(/[^012]/) != nil ? [0] : trinary.chars.reverse.map(&:to_i)
  end

  def to_decimal
    result = 0
    @trinary.each_with_index {|char, index| result += char*(3**index) }
    result
  end
end