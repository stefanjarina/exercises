class School
  VERSION = 1
  def initialize
    @school = Hash.new {|h,k| h[k]=[]}
  end

  def to_h
    Hash[@school.map{|grade, names| [grade, names.sort]}.sort]
  end

  def add(name, grade)
    @school[grade] << name
  end

  def grade(n)
    @school[n].sort
  end
end