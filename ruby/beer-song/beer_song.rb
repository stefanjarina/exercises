class BeerSong
  def verse(i)
    text = ''
    case i
    when 0
      text << "No more bottles of beer on the wall, no more bottles of beer.\n"
      text << "Go to the store and buy some more, 99 bottles of beer on the wall.\n"
    when 1
      text << "1 bottle of beer on the wall, 1 bottle of beer.\n"
      text << "Take it down and pass it around, no more bottles of beer on the wall.\n"
    when 2
      text << "2 bottles of beer on the wall, 2 bottles of beer.\n"
      text << "Take one down and pass it around, 1 bottle of beer on the wall.\n"
    else
      text << "#{i} bottles of beer on the wall, #{i} bottles of beer.\n"
      text << "Take one down and pass it around, #{i-1} bottles of beer on the wall.\n"
    end
    text
  end

  def verses(start, stop)
    song = ''
    start.downto(stop).each {|i| song << verse(i) + "\n"}
    song
  end

  def sing
    verses(99, 0)
  end
end