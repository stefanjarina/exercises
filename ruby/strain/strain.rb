class Array
  def keep
    result = []
    self.each {|n| result << n if yield(n)}
    result
  end

  def discard
    result = []
    self.each {|n| result << n unless yield(n)}
    result
  end
end