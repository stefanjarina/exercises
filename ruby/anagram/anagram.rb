class Anagram
  def initialize(anagram)
    @anagram = anagram
  end

  def match(list)
    return list.select{|w| w if !same(w) && compare(w) }
  end

  private
  def standardize(word)
    word.downcase.chars.sort
  end

  def same(candidate)
    @anagram.downcase == candidate.downcase
  end

  def compare(candidate)
    standardize(@anagram) == standardize(candidate)
  end
end