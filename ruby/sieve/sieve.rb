class Sieve
  def initialize(n)
    @n = n
  end

  def primes
    marked = []
    primes = []
    (2...@n).each do |i|
      next if marked.include? i
      primes << i
      j = i+i
      while j < @n do
        marked << j
        j += i
      end
    end
    primes
  end
end