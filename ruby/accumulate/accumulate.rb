class Array
  def accumulate
    result = []
    self.each {|n| result << yield(n)}
    result
  end
end