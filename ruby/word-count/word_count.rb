class Phrase
  VERSION = 1
  def initialize(phrase)
    @words = phrase.downcase.scan(/\w+'?\w+|\d/)
  end
  def word_count
    @words.each_with_object(Hash.new(0)) {|word, h| h[word] += 1}
  end
end