class Squares
  VERSION = 1
  def initialize(num)
    @num = num
  end

  def square_of_sums
    result = (@num * (@num + 1)) / 2
    result * result
  end

  def sum_of_squares
    (@num * (@num + 1) * (2*@num + 1)) / 6
  end

  def difference
    square_of_sums - sum_of_squares
  end
end