class PrimeFactors
  def self.for(n)
    factors = []
    d = 2
    while n > 1 do
      while n % d == 0 do
        factors << d
        n /= d
      end
      d += 1
      if d*d > n
        factors << n if n > 1
        break
      end
    end
    factors
  end
end