class Bob
  def hey(str)
    return 'Fine. Be that way!' if str.strip == ""
    return 'Whoa, chill out!' if str.match(/[A-Z]/) && str.match(/\p{Lower}/) == nil
    return 'Sure.' if str.end_with?('?')
    return 'Whatever.'
  end
end