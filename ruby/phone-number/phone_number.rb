class PhoneNumber
  attr_reader :number
  def initialize(number)
    if is_valid?(number)
      digits_only = number.gsub(/[^\d]/, "")
      @number = digits_only.length == 10 ? digits_only : digits_only[1..-1]
    else
      @number = '0000000000'
    end
  end

  def area_code
    number[0..2]
  end

  def to_s
    "(#{number[0..2]}) #{number[3..5]}-#{number[6..-1]}"
  end

  private
  def is_valid?(n)
    return false if n.match(/[A-z]/) != nil
    digits = n.gsub(/[^\d]/, "")
    return false if digits.length < 10 || digits.length > 11
    return false if digits.length == 11 && digits[0] != "1"
    return true
  end
end