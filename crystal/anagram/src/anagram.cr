class Anagram
  def self.find(word : String, candidates : Array(String))
    candidates.select { |c|
      word.downcase.chars.sort == c.downcase.chars.sort && word.downcase != c.downcase
    }
  end
end
