class Node
    getter root : Array(Int32)
    getter left
    getter right
    
    def initialize(root : Int32)
        @root = Array(Int32).new
        @root.push(root)
    end
  
    def insert(value)
      if value <= root
        insert_left(value)
      else
        insert_right(value)
      end
    end
  
    def each(&block)
      puts " I was called"
      left && left.each(&block)
      block.call(root)
      right && right.each(&block)
    end
  
    private def insert_left(value)
      if left
        left.insert(value)
      else
        @left = Node.new(value)
      end
    end
  
    private def insert_right(value)
      if right
        right.insert(value)
      else
        @right = Node.new(value)
      end
    end

  end