module Gigasecond
    extend self

    GIGASECOND = (10**9).seconds

    def from(date : Time)
        date + GIGASECOND
    end
end