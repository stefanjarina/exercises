module Binary
  extend self

  def to_decimal(binary : String)
    raise ArgumentError.new unless binary.match(/^[01]+$/)
    result = 0
    binary.chars.reverse.each_with_index { |char, index| result += 2**index if char == '1' }
    result
  end
end
