# Please implement your solution to hamming in this file
class Hamming
  def self.compute(string_a : String, string_b : String)
    raise ArgumentError.new if string_a.size != string_b.size
    (0...string_a.size).count { |i| string_a[i] != string_b[i] }
  end
end
