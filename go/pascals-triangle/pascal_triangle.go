package pascal

func Triangle(n int) (out [][]int) {
	if n < 1 {
		return
	}
	out = make([][]int, n)
	res := []int{1}
	out[0] = res
	for i := 1; i < n; i++ {
		prevRes := res
		res = make([]int, i+1)
		res[0] = 1
		res[i] = 1
		for j := 1; j < i; j++ {
			res[j] = prevRes[j-1] + prevRes[j]
		}
		out[i] = res
	}
	return out
}