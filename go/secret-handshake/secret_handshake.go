package secret

var commands = []string{"wink", "double blink", "close your eyes", "jump"}

func Handshake(n int) (output []string) {
	switch {
	case n < 1 || n > 31:
	case n&16 == 0:
		for i := 0; i <= 3; i++ {
			if n&1 != 0 {
				output = append(output, commands[i])
			}

			n >>= 1
		}
	default:
		for i := 3; i >= 0; i-- {
			if n&8 != 0 {
				output = append(output, commands[i])
			}
			n <<= 1
		}
	}
	return
}