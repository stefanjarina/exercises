package clock

import (
	"fmt"
)

const minutes_in_day = 1440 // 24*60

type Clock struct {
	minutes int
}

func (c *Clock) String() string {
	hours := c.minutes / 60
	minutes := c.minutes % 60
	return fmt.Sprintf("%0.2d:%0.2d", hours, minutes)
}

func (c Clock) Add(m int) Clock {
	minutes := (c.minutes + m) % minutes_in_day
	if minutes < 0 {
		minutes += minutes_in_day
	}
	c.minutes = minutes
	return c
}

func New(h int, m int) Clock {
	minutes := (h*60 + m) % minutes_in_day
	if minutes < 0 {
		minutes += minutes_in_day
	}
	return Clock{minutes}
}