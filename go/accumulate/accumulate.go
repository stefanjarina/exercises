package accumulate

// Accumulate is basically a map function
func Accumulate(acc []string, f func(string) string) []string {
	result := make([]string, len(acc))
	for i, v := range acc {
		result[i] = f(v)
	}
	return result
}
