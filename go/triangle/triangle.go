package triangle

import (
	"math"
)

type Kind string

var (
	Equ Kind = "Equilateral"
	Iso Kind = "Isosceles"
	Sca Kind = "Scalane"
	NaT Kind = "Not a Triangle"
)

func KindFromSides(a, b, c float64) Kind {
	if math.IsNaN(a) || math.IsNaN(b) || math.IsNaN(c) {
		return NaT
	}

	// Sort sides
	if a > b {
		a, b = b, a
	}
	if b > c {
		b, c = c, b
	}
	if a > b {
		a, b = b, a
	}

	switch {
	case a <= 0:
		return NaT
	case a+b <= c:
		return NaT
	case a == b:
		if b == c {
			return Equ
		}
		return Iso
	case a == c || b == c:
		return Iso
	}
	return Sca
}