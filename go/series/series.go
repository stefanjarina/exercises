package slice

func Frist(n int, s string) string {
	return s[0:n]
}

func All(n int, s string) (list []string) {
	for i := 0; i < (len(s) - (n - 1)); i++ {
		list = append(list, s[i:i+n])
	}
	return list
}

func First(n int, s string) (string, bool) {
	if n > len(s) {
		return "", false
	}
	return s[0:n], true
}