package house

func Embed(phrase, endPhrase string) string{
	return phrase + " " + endPhrase
}

func Verse(startPhrase string, phrases []string, endPhrase string) string {
	return startPhrase + " " + recurse(phrases, endPhrase)
}

func recurse(phrases []string, endPhrase string) string {
	if len(phrases) == 0 {
		return endPhrase
	}
	return Embed(phrases[0], recurse(phrases[1:], endPhrase))
}

func Song() string {
	phrases := []string{
			"the horse and the hound and the horn\nthat belonged to",
			"the farmer sowing his corn\nthat kept",
			"the rooster that crowed in the morn\nthat woke",
			"the priest all shaven and shorn\nthat married",
			"the man all tattered and torn\nthat kissed",
			"the maiden all forlorn\nthat milked",
			"the cow with the crumpled horn\nthat tossed",
			"the dog\nthat worried",
			"the cat\nthat killed",
			"the rat\nthat ate",
			"the malt\nthat lay in",
		}
	startPhrase := "This is"
	endPhrase := "the house that Jack built."

	s := startPhrase + " " + endPhrase
	for i := len(phrases) -1 ; i >= 0; i-- {
		s += "\n\n" + Verse(startPhrase, phrases[i:], endPhrase)
	}
	return s
}