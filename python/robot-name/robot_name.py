import os, random, string

class Robot(object):
    def __generate_name(self):
        random.seed()
        name = ""
        name += "".join(random.sample(list(string.ascii_uppercase), 2))
        name += "".join(random.sample([str(x) for x in range(0,10)], 3))
        return name

    def __init__(self):
        self.name = self.__generate_name()
    
    def reset(self):
        self.name = self.__generate_name()
