def hey(text):
    text = text.strip()
    if text == '':
        return 'Fine. Be that way!'
    elif isAsking(text) and isShouting(text):
        return "Calm down, I know what I'm doing!"
    elif isShouting(text):
        return 'Whoa, chill out!'
    elif isAsking(text):
        return 'Sure.'
    else:
        return 'Whatever.'

def isAsking(text):
    return text.endswith('?')

def isShouting(text):
    return text.isupper()