module.exports = {
  "extends": "airbnb-base",
  "plugins": [
      "import"
  ],
  "parserOptions": {
    "ecmaVersion": 6,
    "sourceType": "module"
  },
  "env": {
    "es6": true,
    "node": true,
    "jest": true
  },
  "rules": {
    "import/no-unresolved": "off",
    "import/extensions": "off",
    "linebreak-style": ["error", "windows"]
  }
};
