export default class Transcriptor {
  constructor() {
    this.dnaToRna = {
      G: 'C',
      C: 'G',
      T: 'A',
      A: 'U',
    };
  }

  toRna(dna) {
    if (!dna.match(/^[GCTA]+$/)) {
      throw new Error('Invalid input DNA.');
    }

    return dna.split('').map(d => this.dnaToRna[d]).join('');
  }
}
